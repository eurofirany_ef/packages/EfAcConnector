# EfAcConnector


## Install
```bash
composer require eurofirany/ef-ac-connector
```

##### Publish config

```bash
php artisan vendor:publish --tag=ef_ac_connector
```

## Variables for .env

```bash
AC_LOGIN=
AC_WS1=
AC_WS_PASSWORD=
AC_EFWebService=
AC_EFWebService_PASSWORD=
```
