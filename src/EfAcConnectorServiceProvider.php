<?php

namespace Eurofirany\EfAcConnector;

use Illuminate\Support\ServiceProvider;

class EfAcConnectorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('ef_ac_connector.php'),
            ], 'ef_ac_connector');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'ef_ac_connector');
    }
}
