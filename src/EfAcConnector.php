<?php

namespace Eurofirany\EfAcConnector;

use Sourcetoad\Soapy\SoapyFacade;
use Sourcetoad\Soapy\SoapyCurtain;

class EfAcConnector
{
    private $parameters;

    /** @var SoapyBaseClient */
    private $client;

    /**
     * Api constructor.
     */
    public function __construct()
    {
        $this->overrideParameter();
    }

    /**
     * @param $endpoint
     */
    public function createWrapper(string $endpoint)
    {
        $this->setParameter('KluczSesji', config("ef_ac_connector.password_{$endpoint}"));

        $this->client = SoapyFacade::create(function (SoapyCurtain $curtain) use ($endpoint) {
            return $curtain
                ->setWsdl(config("ef_ac_connector.url.{$endpoint}"))
                ->setCache(WSDL_CACHE_NONE);
        });
    }

    /**
     * @param array $parameters
     */
    public function setParameters($parameters = [])
    {
        foreach ($parameters as $key => $parameter)
            $this->setParameter($key, $parameter);
    }

    /**
     * @param $key
     * @param $value
     */
    public function setParameter($key, $value)
    {
        $this->parameters[$key] = $value;
    }

    /**
     * @param $key
     * @param $value
     */
    public function addParameter($key, $value)
    {
        $this->parameters[$key][] = $value;
    }

    public function overrideParameter()
    {
        $this->clearParameters();
        $this->setParameter('KodKlienta', config('ef_ac_connector.login'));
    }

    public function clearParameters()
    {
        $this->parameters = [];
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param $method
     * @param int $ttl
     * @param int $endpoint
     * @return mixed
     */
    public function request(string $method, string $endpoint = 'EFWebService')
    {
        $this->createWrapper($endpoint);
        $response = $this->client->call($method, $this->getParameters());
        $this->overrideParameter();

        return $response;
    }

}
