<?php

return [
    'login' => env('AC_LOGIN'),
    'password_WS1' => env('AC_WS_PASSWORD'),
    'password_EFWebService' => env('AC_EFWebService_PASSWORD'),
    'url' => [
        'WS1' => env('AC_WS1', ''),
        'EFWebService' => env('AC_EFWebService', '')
    ],
];